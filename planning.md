- [x] Fork and clone the starter project from django-one-shot
- [x] Create a new virtual environment in the repository directory for the project
- [x] Activate the virtual environment
- [x] Install django
- [x] Upgrade pip: python -m pip install --upgrade pip
- [x] Install black
- [x] Install flake8
- [x] Install djlint: pip install djlint
- [x] Install Django debug toolbar: pip install django-debug-toolbar
- [x] Deactivate your virtual environment: decativate
- [x] Activate your virtual environment: source .venv/bin/activate
- [x] Use pip freeze to generate a requirements.txt file: pip freeze > requirements.txt
- [ ] Install dependencies from requirements.txt
      pip install -r requirements.txt
- [x] Add and commit your progress

---

## Resources

https://learn-2.galvanize.com/cohorts/3352/blocks/1859/content_files/build/02-django-one-shot/65-django-one-shot-01.md

## Feature 2

In the repo directory:

- [x] Create a Django project named brain_two so that the manage.py file is in the top directory: django-admin startproject brain_two .
- [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list (in settings.py): python manage.py startapp todos
- [x] Run the migrations
- [x] Create a super user with username(qm), email, and pw(---)

## Feature 3

- [x] to create a TodoList model in the todos Django app with a dunder
- [x] make migrations and migrate your database
- [x] run the django shell: python manage.py shell
- [x] once in the shell:
  - [x] import the model: from todos.models import TodoList
  - [x] and create a few TodoLists: todolist = TodoList.objects.create(name="Reminders")
- [x] exit shell, then add, commit, and push

## Feature 4

- [x] egister the TodoList model with the admin so that you can see the "Todo lists" in the Django admin site: admin.site.register(TodoList)
- [x] To test this, open up the Django admin in your browser and try adding some TodoLists using the admin. You should also see any to-do lists you added at the django shell earlier.
- [x] add, commit, push

## Feature 5

- [x] create a TodoItem model in the todos Django app, then migrate
- [x] to test this model, you can run the django shell: python manage.py shell. Once in the shell:
  - [x] 1.  import the model: from todos.models import TodoList,TodoItem
  - [x] 2.  use the TodoItem.objects.create method to create a few new TodoItems: todolist = TodoList.objects.first(), todoitem = TodoItem.objects.create(task="Take out the Garbage", list=todolist)
- [x] add, commit, push

## Feature 6

- [x] register the TodoItem model with the admin, so that you can see it in the Django admin site
- [x] to test this, open up the Django admin in browser and try adding some TodoItems using the admin, **_you should be able to associate them with your TodoList items you made previously_**
- [x] add, commit, git push origin main

## Feature 7

- [x] create a view that will get all of the instances of the TodoList model and put them in the context for the template.
- [x] register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
- [x] include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
- [x] create a template called todo_list_list.html
- [x] navigateto http://localhost:8000/todos
