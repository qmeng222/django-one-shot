from django.urls import path
from todos.views import *

urlpatterns = [
    path('', todo_list_list, name='todo_list_list'),
    path('<int:pk>/', todo_list_detail, name="todo_list_detail"),
    path('create/', todo_list_create, name="todo_list_create"),
    path("<int:pk>/edit/", todo_list_edit, name="todo_list_edit"),
    path("<int:pk>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:pk>/edit/", todo_item_edit, name="todo_item_edit"),
]
