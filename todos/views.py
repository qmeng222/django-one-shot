from django.shortcuts import get_object_or_404, render, redirect
from todos.models import *
from todos.forms import *

# Create your views here.

def todo_list_create(request):
  context =  {}
  form = TodoListForm(request.POST or None)
  if form.is_valid():
    form.save()
    return redirect("show_books")
  elif TodoListForm:
    form = TodoListForm()
  else:
    form = None

  context["form"] = form
  return render(request, "todos/todo_list_create.html", context)


def todo_list_edit(request, pk):
  todo_list = get_object_or_404(TodoList, pk=pk)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=todo_list)
    if form.is_valid():
      todo_list = form.save()
      return redirect("todo_list_detail", todo_list.pk)
  else:
      todo_list = get_object_or_404(TodoList, pk=pk)
      form = TodoListForm(instance=todo_list)
  return render(request, "todos/todo_list_edit.html", {"form": form})


def todo_list_delete(request, pk):
  todo_list = get_object_or_404(TodoList, pk=pk)
  if request.method == "POST":
    todo_list.delete()
    return redirect("todo_list_list")
  return render(request, "todos/todo_list_delete.html")


def todo_list_list(request):
  todo_lists = TodoList.objects.all()
  context = {
    'todo_lists': todo_lists
  }
  return render(request, 'todos/todo_list_list.html', context)

def todo_list_detail(request, pk):
  todo_list = get_object_or_404(TodoList, pk=pk)
  return render(request, "todos/todo_list_detail.html", {"todo_list": todo_list})


def todo_item_create(request):
  if request.method == "POST":
    form = TodoItemForm(request.POST)
    if form.is_valid():
      todo_item = form.save()
      return redirect("todo_list_detail", todo_item.list.pk)
  else:
    form = TodoItemForm()
  return render(request, "todos/todo_item_create.html", {"form": form})


def todo_item_edit(request, pk):
    todo_item = get_object_or_404(TodoItem, pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todo_item.list.pk)
    else:
        form = TodoItemForm(instance=todo_item)
    return render(request, "todos/todo_item_edit.html", {"form": form})
